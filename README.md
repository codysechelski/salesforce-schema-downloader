## About The Project

A utility written in Python for quickly extracting object schema data from Salesforce and saving it in Microsoft Excel format

### Built With

Python 3.9+

## Getting Started

To get a local copy up and running follow these simple steps.

1. Clone this repository or download the source files.
2. Ensure Python 3.9+ is installed
3. Install the required libraries (see the Prerequisite section)

### Prerequisites

- Python 3.9+
- [Requests](https://pypi.org/project/requests/) Library
- [XlsxWriter](https://pypi.org/project/XlsxWriter/) Library
- [Salesforce CLI](https://developer.salesforce.com/tools/salesforcecli)

### Installation

1. Clone the repo
   ```sh
   git clone git@bitbucket.org:codysechelski/salesforce-schema-downloader.git
   ```
2. Install Requests
   ```sh
   pip3 install requests
   ```
3. Install XlsxWriter
   ```sh
   pip3 install xlsxwriter
   ```

### Config

1. Open the `environments.py` file.
2. Set the `cli_alias` for each environment you plan to use
   1. The value should be the Salesforce CLI Alias for the authorized org for the environment
3. Set the `base_path` for each environment you plan yo use
   1. The value should be the "non-lighting" base path for the org. For example: `https://dev.sandbox.my.salesforce.com`, **NOT** `https://dev.sandbox.lightning.force.com`. An easy way to get the correct base path is to click on your avitar at the top right in Salesforce. In the popover that opens, the url is right below your name. Don't forget to add the `https://` in front of it.

Feel free to add or remove any environments in this file to fit your needs. However, there is no harm in leaving unused environments in the file with blank values.


## Usage

Before you run the code, you should check, and if necessary, change the target environment. Open the `sf_describe.py` file and fine the line, towards the top where the environment is specified. It should look something like this...

```python
ENV = Environment.DEV
```

You can change which environment to use by changing the value after the `.`.

From a terminal, navigate to the directory created when you cloned this repo and run the `sf_describe.py` file

```sh
python3 sf_describe.py
```

Follow the on screen prompts

## Roadmap

1. Add more error cheking
2. Add testing
3. Adding a feature to list all object for an org
4. Option to download the schema for all objects using a wildcard `*` or something
5. Implement a feature to allow for fuzzy object name matching
6. Integrate SFDX-CLI for authentication and describe
7. Support some sort of structured format that could be imported into Vizio or something like it to automatically build a graphical ERD

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

Cody Sechelski - codysechelski@gmail.com

Project Link: [https://bitbucket.org/codysechelski/salesforce-schema-downloader/](https://bitbucket.org/codysechelski/salesforce-schema-downloader/)
