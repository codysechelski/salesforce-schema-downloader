import subprocess
import json
import requests
from datetime import datetime
from requests.utils import requote_uri
from environments import Environment


class SFTools:

    api_version = ""
    environment_name = ""
    base_path = ""
    cli_alias = ""
    base_path = ""
    session_id = ""
    verbose_output = False

    def __init__(self, environment, api_version, verbose) -> None:
        self.environment_name = environment["name"]
        self.base_path = environment["base_path"]
        self.cli_alias = environment["cli_alias"]
        self.api_version = api_version
        self.verbose_output = verbose

        self.session_id = self.__get_session_id__(self.cli_alias)

    def __get_session_id__(self, alias):
        result = subprocess.run(
            ['sfdx', 'force:org:open', '-u', alias, '-r', '--json'], stdout=subprocess.PIPE)
        result_out = json.loads(result.stdout)

        if self.verbose_output:
            print(result_out)

        return result_out['result']['url'].split('sid=')[1]

    def __format_resource_path__(self, resource_path):
        if resource_path.startswith('/services/data/v'):
            return resource_path
        else:
            if resource_path.startswith('/'):
                return f"/services/data/v{self.api_version}{resource_path}"
            else:
                return f"/services/data/v{self.api_version}/{resource_path}"

    def call_api(self, resource_path, method='GET', additional_headers=None, body=None) -> requests.Response:
        endpoint = f"{self.base_path}{self.__format_resource_path__(resource_path)}"
        headers = {
            'Authorization': f"Bearer {self.session_id}",
            "Content-Type": "application/json"
        }

        if additional_headers:
            headers.update(additional_headers)

        if self.verbose_output:
            print(f"endpoint = {endpoint}")
            print(f"headers = {headers}")

        resp = None

        if self.verbose_output:
            print('endpoint = ' + endpoint)
            print('method = ' + method)
            print(body)

        if method.upper() == 'GET':
            resp = requests.get(endpoint, headers=headers)
        elif method.upper() == 'POST':
            resp = requests.post(endpoint, headers=headers, json=body)
        elif method.upper() == 'PUT':
            resp = requests.put(endpoint, headers=headers, json=body)
        elif method.upper() == 'PATCH':
            resp = requests.patch(endpoint, headers=headers, json=body)
        elif method.upper() == 'DELETE':
            resp = requests.delete(endpoint, headers=headers)

        if self.verbose_output:
            print(resp.status_code)
            print(resp.json())
        return resp

    def query(self, query, use_tooling_api=False, include_deleted_and_archived=False):
        records = []

        aggregate_key_words = [
            "avg(",
            "count(",
            "count_distinct(",
            "min(",
            "max(",
            "sum("
        ]

        is_aggregate_query = False

        for key_word in aggregate_key_words:
            if key_word in query.lower():
                is_aggregate_query = True
                if self.verbose_output:
                    print("Aggregate Query Keyword found.")
                break

        tooling_clause = 'tooling/' if use_tooling_api else ''
        query_clause = 'queryAll' if include_deleted_and_archived else 'query'

        endpoint = f"{self.base_path}/services/data/v{self.api_version}/{tooling_clause}{query_clause}?q={requote_uri(query)}"

        if self.verbose_output:
            print(f'Endpoint: {endpoint}')

        headers = {
            'Authorization': f"Bearer {self.session_id}"
        }

        counter = 0
        done = False
        if is_aggregate_query:
            resp = requests.get(endpoint, headers=headers)
            if resp.status_code == 200:
                done = True
                return resp.json()["totalSize"]
            else:
                raise RuntimeError(
                    f"\nHTTP CODE: {str(resp.status_code)}\nERROR MESSAGE: {data[0]['message']}")
        else:
            while not done:
                resp = requests.get(endpoint, headers=headers)
                data = resp.json()
                if resp.status_code == 200:

                    records.extend(data['records'])

                    if not data['done']:
                        endpoint = f"{self.base_path}{data['nextRecordsUrl']}"
                    else:
                        done = True

                    counter += 1

                    if self.verbose_output:
                        print(f"Batch {counter:05} - {len(records)} so far.")
                else:
                    raise RuntimeError(
                        f"\nHTTP CODE: {str(resp.status_code)}\nERROR MESSAGE: {data[0]['message']}")

        return records
