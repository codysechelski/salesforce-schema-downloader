class Environment:
    DEV = {
        "name": "Dev",
        "cli_alias": "mecano-fivesky-dev",
        "base_path": "https://fivesky--mecano016.sandbox.my.salesforce.com"
    }

    QA = {
        "name": "QA",
        "cli_alias": "my-qa-alias",
        "base_path": "https://....sandbox.my.salesforce.com"
    }

    UAT = {
        "name": "UAT",
        "cli_alias": "my-uat-alias",
        "base_path": "https://....sandbox.my.salesforce.com"
    }

    PROD = {
        "name": "Production",
        "cli_alias": "my-prod-alias",
        "base_path": "https://.....salesforce.com"
    }
